import 'package:flutter/material.dart';

class CustomTextStyle {
  static const TextStyle textStyle17 = TextStyle(
    fontFamily: 'Roboto',
    fontSize: 17,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const TextStyle textStyle15 = TextStyle(
    fontFamily: 'Roboto',
    fontSize: 15,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const TextStyle textStyle13 = TextStyle(
    fontFamily: 'Roboto',
    fontSize: 13,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const TextStyle textStyle22 = TextStyle(
    fontFamily: 'Ubuntu',
    fontSize: 22,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const TextStyle textStyle28 = TextStyle(
    fontFamily: 'Ubuntu',
    fontSize: 28,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const TextStyle textStyle32 = TextStyle(
    fontFamily: 'Ubuntu',
    fontSize: 32,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const TextStyle textStyle64 = TextStyle(
    fontFamily: 'Ubuntu',
    fontSize: 64,
    leadingDistribution: TextLeadingDistribution.even,
  );
}
