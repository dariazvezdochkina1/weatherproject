class Sizing {
  Sizing._();

  static const size8 = 8.0;
  static const size12 = 12.0;
  static const size15 = 15.0;
  static const size16 = 16.0;
  static const size17 = 17.0;
  static const size18 = 18.0;
  static const size24 = 24.0;
  static const size28 = 28.0;
  static const size48 = 48.0;
  static const size64 = 64.0;
  static const size86 = 86.0;
  static const size150 = 150.0;

  static const radius20 = 20.0;
  static const radius24 = 24.0;
  static const radius40 = 40.0;
}
