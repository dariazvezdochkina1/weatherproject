class WeatherModel {
  final int temperature;
  final int minTemp;
  final int maxTemp;
  final String description;
  final int humidity;
  final int wind;
  final int windDegree;
  final String country;
  final String city;

  WeatherModel({
    required this.temperature,
    required this.minTemp,
    required this.maxTemp,
    required this.description,
    required this.humidity,
    required this.wind,
    required this.windDegree,
    required this.country,
    required this.city,
  });
}
