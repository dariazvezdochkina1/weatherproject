import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../../data/model/weather_model.dart';
import '../state/weather_state.dart';

class WeatherCubit extends Cubit<WeatherState> {
  WeatherCubit() : super(WeatherInitial());

  Future<void> fetchWeather(double latitude, double longitude) async {
    try {
      emit(WeatherLoading());
      const apiKey = 'aa5152078cb4339ebbce5cae3c33377d';
      final url =
          'https://api.openweathermap.org/data/2.5/weather?lat=$latitude&lon=$longitude&appid=$apiKey';

      final response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        final weatherData = jsonDecode(response.body);
        final mainData = weatherData['main'];
        final weather = WeatherModel(
          temperature: mainData['temp'].toInt(),
          minTemp: mainData['temp_min'].toInt(),
          maxTemp: mainData['temp_max'].toInt(),
          description: weatherData['weather'][0]['main'],
          humidity: mainData['humidity'].toInt(),
          wind: weatherData['wind']['speed'].toInt(),
          windDegree: weatherData['wind']['deg'].toInt(),
          country: weatherData['sys']['country'],
          city: weatherData['name'],
        );
        emit(
          WeatherLoaded(weatherModel: weather),
        );
      } else {
        emit(
          const WeatherError('Failed to fetch weather data'),
        );
      }
    } catch (e) {
      emit(
        WeatherError('Failed to fetch weather data: $e'),
      );
    }
  }
}
