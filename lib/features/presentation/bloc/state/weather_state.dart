import 'package:equatable/equatable.dart';
import 'package:weather_test_assignment/features/data/model/weather_model.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();

  @override
  List<Object> get props => [];
}

class WeatherInitial extends WeatherState {}

class WeatherLoading extends WeatherState {}

class WeatherLoaded extends WeatherState {
  final WeatherModel weatherModel;

  const WeatherLoaded({required this.weatherModel});

  @override
  List<Object> get props => [weatherModel];

  @override
  String toString() => 'WeatherLoaded { weatherModel: $weatherModel }';
}

class WeatherError extends WeatherState {
  final String errorMessage;

  const WeatherError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];

  @override
  String toString() => 'WeatherError { errorMessage: $errorMessage }';
}
