import 'dart:async';

import 'package:flutter/material.dart';
import 'package:weather_test_assignment/features/presentation/pages/authentication_page.dart';

import '../../../constants/sizing.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future.delayed(
      const Duration(seconds: 3),
      () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => const AuthenticationPage()));
      },
    );
    return Scaffold(
      body: Center(
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color.fromRGBO(3, 0, 255, 1),
                Colors.black,
              ],
            ),
          ),
          child: Column(
            children: [
              Expanded(
                child: Center(
                  child: Text(
                    'Weather \nService'.toUpperCase(),
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                      fontSize: Sizing.size48,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(bottom: Sizing.size86),
                child: Text(
                  'dawn is coming soon',
                  style: TextStyle(
                    fontSize: Sizing.size24,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w300,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
