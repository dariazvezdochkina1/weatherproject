import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_test_assignment/features/data/model/weather_model.dart';
import 'package:weather_test_assignment/features/presentation/widgets/widgets.dart';


import '../bloc/cubit/weather_cubit.dart';
import '../bloc/state/weather_state.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final weatherCubit = context.read<WeatherCubit>();
    final positionFuture = _getCurrentLocation();

    return Scaffold(
      body: Center(
        child: FutureBuilder<Position>(
          future: positionFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final position = snapshot.data!;
              weatherCubit.fetchWeather(position.latitude, position.longitude);

              return BlocBuilder<WeatherCubit, WeatherState>(
                builder: (context, state) {
                  if (state is WeatherLoading) {
                    return const CircularProgressWidget();
                  } else if (state is WeatherLoaded) {
                    return _buildLoadedWidget(state.weatherModel);
                  } else if (state is WeatherError) {
                    return Text('Error: ${state.errorMessage}');
                  } else {
                    return const Text('Tap the button to fetch weather data.');
                  }
                },
              );
            } else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else {
              return CircularProgressWidget();
            }
          },
        ),
      ),
    );
  }

  Widget _buildLoadedWidget(WeatherModel weather) {
    return HomePageWidget(
      temp: weather.temperature,
      minTemp: weather.minTemp,
      maxTemp: weather.maxTemp,
      description: weather.description,
      humidity: weather.humidity,
      wind: weather.wind,
      country: weather.country,
      city: weather.city,
      windDegree: weather.windDegree,
    );
  }

  Future<Position> _getCurrentLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      throw Exception('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      throw Exception(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        throw Exception(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
}
