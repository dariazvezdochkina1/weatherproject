import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_test_assignment/constants/sizing.dart';
import 'package:weather_test_assignment/features/presentation/pages/home_page.dart';
import 'package:weather_test_assignment/features/presentation/widgets/widgets.dart';

import '../bloc/cubit/weather_cubit.dart';

class AuthenticationPage extends StatefulWidget {
  const AuthenticationPage({Key? key}) : super(key: key);

  @override
  State<AuthenticationPage> createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage> {
  final TextEditingController _passwordController = TextEditingController();

  final TextEditingController _emailController = TextEditingController();

  String? _emailErrorText;
  String? _passwordErrorText;

  Future<void> logIn() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: _emailController.text,
        password: _passwordController.text,
      );

      setState(
        () {
          _emailErrorText = null;
          _passwordErrorText = null;
        },
      );
      WidgetsBinding.instance.addPostFrameCallback(
        (_) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return BlocProvider(
                  child: const HomePage(),
                  create: (context) => WeatherCubit(),
                );
              },
            ),
          );
        },
      );
    } catch (error) {
      setState(
        () {
          if (error is FirebaseAuthException) {
            if (error.code == 'invalid-email') {
              _emailErrorText = 'Invalid email address.';
              _passwordErrorText = null;
            } else if (error.code == 'weak-password') {
              _emailErrorText = null;
              _passwordErrorText =
                  'Weak password. Please choose a stronger password.';
            } else {
              _emailErrorText = null;
              _passwordErrorText = 'An error occurred. Please try again later.';
            }
          } else {
            _emailErrorText = null;
            _passwordErrorText = error.toString();
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(
              left: Sizing.size24, right: Sizing.size24, top: Sizing.size48),
          child: Column(
            children: [
              const HeaderTextBlockWidget(
                  title: "Вход", subtitle: "Введите данные для входа "),
              const SizedBox(height: Sizing.size24),
              TextFieldWidget(
                text: "Email",
                isPassword: false,
                controller: _emailController,
                errorText: _emailErrorText,
              ),
              const SizedBox(height: Sizing.size8),
              TextFieldWidget(
                text: "Пароль",
                icon: "assets/icons/ic_open_eye.svg",
                passwordVisibleIcon: 'assets/icons/ic_closed_eye.svg',
                isPassword: true,
                controller: _passwordController,
                errorText: _passwordErrorText,
              ),
              const SizedBox(height: Sizing.size48),
              LoginButton(
                text: "Войти",
                backgroundColor: const Color.fromRGBO(7, 0, 255, 1),
                callback: () {
                  logIn();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
