import 'package:flutter/material.dart';

import '../../../style/custom_text_style.dart';

class HeaderTextBlockWidget extends StatelessWidget {
  const HeaderTextBlockWidget({
    required this.title,
    required this.subtitle,
    this.alignment = CrossAxisAlignment.start,
    this.rowAlignment = MainAxisAlignment.start,
    Key? key,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final CrossAxisAlignment alignment;
  final MainAxisAlignment rowAlignment;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: rowAlignment,
      children: [
        Column(
          crossAxisAlignment: alignment,
          children: [
            Text(
              title,
              style: CustomTextStyle.textStyle28.copyWith(
                color: const Color.fromRGBO(43, 45, 51, 1),
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(height: 12),
            Text(
              subtitle,
              style: CustomTextStyle.textStyle15.copyWith(
                color: const Color.fromRGBO(135, 153, 165, 1),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
