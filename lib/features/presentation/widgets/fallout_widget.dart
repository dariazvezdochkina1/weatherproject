import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../constants/sizing.dart';
import '../../../style/custom_text_style.dart';

class FalloutWidget extends StatelessWidget {
  final int wind;
  final int humidity;
  final String humidityMessage;
  final String windMessage;

  const FalloutWidget({
    Key? key,
    required this.wind,
    required this.humidity,
    required this.humidityMessage,
    required this.windMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle15WhiteOpacity02 = CustomTextStyle.textStyle15.copyWith(
      color: Colors.white.withOpacity(0.2),
    );
    final textStyle15White = CustomTextStyle.textStyle15.copyWith(
      color: Colors.white,
    );

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Sizing.radius20),
        color: Colors.white.withOpacity(0.2),
      ),
      child: Padding(
        padding: const EdgeInsets.all(Sizing.size16),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset('assets/icons/ic_wind.svg'),
                        const SizedBox(width: Sizing.size8),
                        Text(
                          "$wind м/с",
                          style: textStyle15WhiteOpacity02,
                        ),
                      ],
                    ),
                    const SizedBox(height: Sizing.size18),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SvgPicture.asset('assets/icons/ic_humidity.svg'),
                        const SizedBox(width: Sizing.size8),
                        Text(
                          "$humidity %",
                          style: textStyle15WhiteOpacity02,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(width: Sizing.size24),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      windMessage,
                      style: textStyle15White,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: Sizing.size18),
                    Text(
                      humidityMessage,
                      style: textStyle15White,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
