import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TextFieldWidget extends StatefulWidget {
  final String text;
  final String? errorText;
  final String? icon;
  final String? passwordVisibleIcon;
  final bool isPassword;
  final TextEditingController controller;

  const TextFieldWidget({
    Key? key,
    required this.text,
    this.errorText,
    this.icon,
    required this.isPassword,
    required this.controller,
    this.passwordVisibleIcon,
  }) : super(key: key);

  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  bool isPasswordVisible = true;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      obscureText: widget.isPassword ? isPasswordVisible : false,
      enableSuggestions: !widget.isPassword,
      autocorrect: !widget.isPassword,
      cursorColor: Colors.red,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        suffixIcon: (widget.icon != null || widget.passwordVisibleIcon != null)
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    isPasswordVisible = !isPasswordVisible;
                  });
                },
                child: Container(
                  padding: const EdgeInsets.all(8),
                  child: SvgPicture.asset(
                    isPasswordVisible
                        ? widget.passwordVisibleIcon!
                        : widget.icon!,
                  ),
                ),
              )
            : null,
        errorText: widget.errorText,
        errorStyle: const TextStyle(color: Colors.red),
        labelText: widget.text,
        labelStyle: const TextStyle(color: Color.fromRGBO(135, 153, 165, 1)),
      ),
      keyboardType: widget.isPassword
          ? TextInputType.visiblePassword
          : TextInputType.emailAddress,
    );
  }
}
