import 'package:flutter/material.dart';

import '../../../constants/sizing.dart';

class LoginButton extends StatelessWidget {
  final String text;
  final VoidCallback callback;
  final Color backgroundColor;

  const LoginButton({
    required this.text,
    required this.callback,
    required this.backgroundColor,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SizedBox(
            height: Sizing.size48,
            child: ElevatedButton(
              onPressed: callback,
              style: ElevatedButton.styleFrom(
                  backgroundColor: backgroundColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(Sizing.radius24))),
              child: Text(text),
            ),
          ),
        ),
      ],
    );
  }
}
