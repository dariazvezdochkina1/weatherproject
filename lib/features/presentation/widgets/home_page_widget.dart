import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weather_test_assignment/style/custom_text_style.dart';
import 'package:weather_test_assignment/features/presentation/widgets/widgets.dart';

import '../../../constants/sizing.dart';

class HomePageWidget extends StatelessWidget {
  final int temp;
  final int minTemp;
  final int maxTemp;
  final int humidity;
  final int wind;
  final int windDegree;
  final String description;
  final String country;
  final String city;

  const HomePageWidget({
    required this.temp,
    required this.minTemp,
    required this.maxTemp,
    required this.description,
    required this.humidity,
    required this.wind,
    required this.country,
    required this.city,
    required this.windDegree,
    Key? key,
  }) : super(key: key);

  int kelvinToCelsius(int kelvin) {
    return (kelvin - 273.15).toInt();
  }

  String getWeatherIconPath(String weatherMain) {
    switch (weatherMain) {
      case 'Clear':
        return 'assets/images/img_clear.png';
      case 'Clouds':
        return 'assets/images/img_clouds.png';
      case 'Rain':
        return 'assets/images/img_rain.png';
      case 'Drizzle':
        return 'assets/images/img_drizzle.png';
      case 'Snow':
        return 'assets/images/img_snow.png';
      case 'Thunderstorm':
        return 'assets/images/img_thunderstorm.png';
      default:
        return '';
    }
  }

  Widget buildHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SvgPicture.asset('assets/icons/ic_pin.svg'),
        Text(
          '$city, $country',
          style: CustomTextStyle.textStyle17.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }

  Widget buildWeatherIcon() {
    String mainWeather = description;
    String weatherIconPath = getWeatherIconPath(mainWeather);

    return Container(
      width: Sizing.size150,
      height: Sizing.size150,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(Sizing.size150),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(188, 135, 255, 1),
            blurRadius: Sizing.radius40,
          ),
        ],
      ),
      child: Image.asset(weatherIconPath),
    );
  }

  Widget buildTemperature() {
    return Text(
      '${kelvinToCelsius(temp)}°C',
      style: CustomTextStyle.textStyle64.copyWith(color: Colors.white),
    );
  }

  Widget buildTemperatureDetails() {
    return Column(
      children: [
        Text(
          description,
          style: CustomTextStyle.textStyle17.copyWith(color: Colors.white),
        ),
        const SizedBox(height: Sizing.size8),
        Text(
          'Макс: ${kelvinToCelsius(maxTemp)}°C Мин: ${kelvinToCelsius(minTemp)}°C',
          style: CustomTextStyle.textStyle17.copyWith(color: Colors.white),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    String humidityMessage;
    String windMessage;

    if (humidity > 80) {
      humidityMessage = 'Высокая влажность';
    } else if (humidity < 40) {
      humidityMessage = 'Низкая влажность';
    } else {
      humidityMessage = 'Умеренная влажность';
    }

    if (windDegree >= 337.5 || windDegree < 22.5) {
      windMessage = 'Северный ветер';
    } else if (windDegree >= 22.5 && windDegree < 67.5) {
      windMessage = 'Северо-восточный ветер';
    } else if (windDegree >= 67.5 && windDegree < 112.5) {
      windMessage = 'Восточный ветер';
    } else if (windDegree >= 112.5 && windDegree < 157.5) {
      windMessage = 'Юго-восточный ветер';
    } else if (windDegree >= 157.5 && windDegree < 202.5) {
      windMessage = 'Южный ветер';
    } else if (windDegree >= 202.5 && windDegree < 247.5) {
      windMessage = 'Юго-западный ветер';
    } else if (windDegree >= 247.5 && windDegree < 292.5) {
      windMessage = 'Западный ветер';
    } else if (windDegree >= 292.5 && windDegree < 337.5) {
      windMessage = 'Северо-западный ветер';
    } else {
      windMessage = 'Неизвестное направление ветра';
    }

    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(5, 2, 171, 1.0),
            Colors.black,
          ],
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          left: Sizing.size24,
          right: Sizing.size24,
          top: Sizing.size48,
        ),
        child: Column(
          children: [
            buildHeader(),
            const SizedBox(height: Sizing.size24),
            buildWeatherIcon(),
            const SizedBox(height: Sizing.size8),
            buildTemperature(),
            buildTemperatureDetails(),
            const SizedBox(height: Sizing.size24),
            FalloutWidget(
              wind: wind,
              humidity: humidity,
              humidityMessage: humidityMessage,
              windMessage: windMessage,
            ),
          ],
        ),
      ),
    );
  }
}
